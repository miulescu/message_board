<?php declare(strict_types=1);

namespace App\Service;

use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * Class ApiTokenService
 * @package App\Service
 */
final class ApiTokenService
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    /**
     * @var JWTEncoderInterface
     */
    private $jwtEncoder;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ApiTokenService constructor.
     *
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @param JWTEncoderInterface $jwtEncoder
     * @param UserRepository $userRepository
     */
    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder, JWTEncoderInterface $jwtEncoder, UserRepository $userRepository)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->jwtEncoder = $jwtEncoder;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    public function generateToken(Request $request): array
    {
        // This will fetch the user from the db.
        $user = $this->userRepository->findOneBy(['email' => $request->getUser()]);

        if (!$user) {
            throw new NotFoundHttpException(sprintf('The user %s does not exist.', $request->getUser()));
        }

        // Check if the password from request is the same as the encoded password from db.
        $isValid = $this->userPasswordEncoder->isPasswordValid($user, $request->getPassword());

        if (!$isValid) {
            throw new BadCredentialsException(sprintf('Invalid password for user %s', $request->getUser()));
        }

        return [
            'success' => true,
            'token' => $this->jwtEncoder->encode(['email' => $user->getEmail()]),
        ];
    }
}