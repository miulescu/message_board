<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\Attachment;
use App\Entity\Post;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ApiService
 * @package App\Service
 */
final class ApiService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;


    /**
     * ApiService constructor.
     *
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     */
    public function __construct(ValidatorInterface $validator, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @param array|null $data
     *
     * @return array|null
     */
    public function registerUser(?array $data): ?array
    {
        $user = new User();
        $user->setRoles(['ROLE_API'])
            ->setEmail($data['email'] ?? null)
            ->setPassword(isset($data['password']) ? $this->userPasswordEncoder->encodePassword($user, $data['password']) : null);

        $errors = $this->validate($user);
        if ($errors) {
            return [
                'success' => false,
                'message' => $errors
            ];
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return null;
    }

    /**
 * @param User $user
 * @param int $page
 *
 * @return array
 *
 * @throws \Exception
 */
    public function getUserPosts(User $user, int $page): array
    {
        $paginator = $this->entityManager->getRepository(Post::class)->findByUser($user, $page);

        if ($paginator->getIterator()->count()) {
            $posts = [];
            foreach ($paginator->getIterator() as $post) {
                $posts[] = $post;
            }
        }

        if (empty($posts)) {
            throw new \Exception('No posts found for current user');
        }

        return [
            'success' => true,
            'data' => [
                'posts' => $posts,
                'totalPages' => round($paginator->count() / 5),
            ],
        ];
    }

    /**
     * @param int $page
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getPosts(int $page): array
    {
        $paginator = $this->entityManager->getRepository(Post::class)->findByPage($page);

        if ($paginator->getIterator()->count()) {
            $posts = [];
            foreach ($paginator->getIterator() as $post) {
                $posts[] = $post;
            }
        }

        if (empty($posts)) {
            throw new \Exception('No posts found');
        }

        return [
            'success' => true,
            'data' => [
                'posts' => $posts,
                'totalPages' => round($paginator->count() / 5),
            ],
        ];
    }

    /**
     * @param User $user
     * @param array|null $data
     *
     * @return array|null
     */
    public function createPost(User $user, ?array $data): ?array
    {
        $post = new Post();
        $post->setTitle($data['title'] ?? null)
            ->setMessage($data['message'] ?? null)
            ->setUser($user);

        $errors = $this->validate($post);
        if ($errors) {
            return [
                'success' => false,
                'message' => $errors
            ];
        }

        if (isset($data['attachments'])) {
            foreach ($data['attachments'] as $attachmentArray) {
                $url = $attachmentArray['url'];
                $attachmentName = $attachmentArray['attachmentName'];
                $attachmentsDir = '../../templates/attachments';

                if (!mkdir($attachmentsDir . "/{$user->getId()}")
                    && !is_dir($attachmentsDir . "/{$user->getId()}")) {
                    return [
                        'success' => false,
                        'message' => 'Cannot create a directory to save the attachments'
                    ];
                }
                // This copy function actually save the file
                if (copy($url, $attachmentsDir . "/{$user->getId()}/$attachmentName")) {
                    $attachment = new Attachment();

                    $attachment->setPath($attachmentsDir . "/{$user->getId()}/$attachmentName")
                        ->setUrl($url)
                        ->setPost($post);

                    $this->entityManager->persist($attachment);
                }
            }
        }

        $this->entityManager->persist($post);
        $this->entityManager->flush();

        return null;
    }

    /**
     * @param $entity
     *
     * @return array|null
     */
    private function validate($entity): ?array
    {
        // This will validate the assertions I wrote on the entity and will return a ConstraintViolationList object
        $errors = $this->validator->validate($entity);

        if ($errors->count()) {
            $errorMessages = [];
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $errorMessages[] = "{$error->getPropertyPath()}: {$error->getMessage()}";
            }
        }

        return $errorMessages ?? null;
    }
}