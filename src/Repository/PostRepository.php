<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * @param $user
     * @param int $currentPage
     *
     * @return Paginator
     */
    public function findByUser($user, int $currentPage): Paginator
    {
        $query = $this->createQueryBuilder('p')
            ->select('p.id, p.title, p.message')
            ->join('p.user', 'u')
            ->addSelect('u.email as user')
            ->where('p.user = :user')
            ->setParameter('user', $user)
            ->orderBy('p.id', 'ASC')
            ->getQuery();

        return $this->paginate($query, $currentPage);
    }

    /**
     * @param int $currentPage
     *
     * @return Paginator
     */
    public function findByPage(int $currentPage): Paginator
    {
        $query = $this->createQueryBuilder('p')
            ->select('p.id, p.title, p.message')
            ->join('p.user', 'u')
            ->addSelect('u.email as user')
            ->orderBy('p.id', 'ASC')
            ->getQuery();

        return $this->paginate($query, $currentPage);
    }

    /**
     * @param Query $query
     * @param int $page
     * @param int $limit
     *
     * @return Paginator
     */
    private function paginate(Query $query, int $page = 1, int $limit = 5): Paginator
    {
        $paginator = new Paginator($query, false);
        $paginator->setUseOutputWalkers(false);

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit

        return $paginator;
    }
}
