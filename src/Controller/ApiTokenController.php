<?php

namespace App\Controller;

use App\Service\ApiTokenService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class ApiTokenController
 * @package App\Controller
 */
class ApiTokenController extends AbstractController
{
    /**
     * @Route("/api/v1/tokens", name="api_v1_post_tokens", methods={"POST"}, defaults={"_format": "json"})
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the token for a user. EG: {'token': 'yJ0eXAiOiJKV1QiLCJhbGc'}",
     * )
     *
     * @SWG\Parameter(
     *     name="username",
     *     in="body",
     *     type="string",
     *     description="The email of the user",
     *     @SWG\Schema(type="string")
     * )
     * @SWG\Parameter(
     *     name="password",
     *     in="body",
     *     type="string",
     *     description="The password of the user",
     *     @SWG\Schema(type="string")
     * )
     * @SWG\Tag(name="Login")
     *
     * @param Request $request
     * @param ApiTokenService $apiTokenService
     *
     * @return JsonResponse
     */
    public function postGenerateToken(Request $request, ApiTokenService $apiTokenService): JsonResponse
    {
        try {
            $token = $apiTokenService->generateToken($request);
        } catch (\Throwable $t) {
            return $this->json(
                [
                    'success' => false,
                    'message' => $t->getMessage(),
                ],
                $t->getCode()
            );
        }

        return $this->json($token, Response::HTTP_CREATED);
    }
}