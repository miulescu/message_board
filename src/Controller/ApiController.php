<?php

namespace App\Controller;

use App\Service\ApiService;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * Class ApiController
 * @package App\Controller
 */
class ApiController extends AbstractController
{
    /**
     * @Route("api/v1/register", name="api_v1_post_register", methods={"POST"}, defaults={"_format": "json"})
     *
     * @SWG\Response(
     *     response=201,
     *     description="Register a new username. Eg: {'success' : true, 'message': 'The user was successfully created'}",
     * )
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     type="string",
     *     description="The email of the user",
     *     @SWG\Schema(type="string")
     * )
     * @SWG\Parameter(
     *     name="password",
     *     in="body",
     *     type="string",
     *     description="The password for the user",
     *     @SWG\Schema(type="string")
     * )
     * @SWG\Tag(name="Register")
     *
     * @param Request $request
     * @param ApiService $apiService
     *
     * @return JsonResponse
     */
    public function postRegisterUser(Request $request, ApiService $apiService): JsonResponse
    {
        $errors = $apiService->registerUser(json_decode($request->getContent(), true));

        if ($errors) {
            return $this->json($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->json(
            [
                'success' => true,
                'message' => 'The user was successfully created.',
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route(
     *     "api/v1/user_posts/{page}",
     *     name="api_v1_get_user_posts",
     *     methods={"GET"},
     *     requirements={"page"="\d+"},
     *     defaults={"_format": "json", "page": 1}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Get all the posts by logged in user.",
     * )
     *
     * @SWG\Parameter(
     *     name="page",
     *     in="path",
     *     type="string",
     *     description="The current page number"
     * )
     *
     * @SWG\Tag(name="Posts")
     * @Security(name="Bearer")
     *
     * @param ApiService $apiService
     * @param int $page
     *
     * @return JsonResponse
     */
    public function getUserPosts(ApiService $apiService, int $page): JsonResponse
    {
        try {
            $json = $apiService->getUserPosts($this->getUser(), $page);

            return $this->json($json, Response::HTTP_OK);
        } catch (\Throwable $t) {
            return $this->json(
                [
                    'success' => false,
                    'message' => $t->getMessage(),
                ],
                Response::HTTP_NOT_FOUND
            );
        }
    }

    /**
     * @Route(
     *     "api/v1/posts/{page}",
     *     name="api_v1_get_posts",
     *     methods={"GET"},
     *     requirements={"page"="\d+"},
     *     defaults={"_format": "json", "page": 1}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Get all the posts.",
     * )
     *
     * @SWG\Parameter(
     *     name="page",
     *     in="path",
     *     type="string",
     *     description="The current page number"
     * )
     *
     * @SWG\Tag(name="Posts")
     * @Security(name="Bearer")
     *
     * @param ApiService $apiService
     * @param int $page
     *
     * @return JsonResponse
     */
    public function getPosts(ApiService $apiService, int $page): JsonResponse
    {
        try {
            $json = $apiService->getPosts($page);

            return $this->json($json, Response::HTTP_OK);
        } catch (\Throwable $t) {
            return $this->json(
                [
                    'success' => false,
                    'message' => $t->getMessage(),
                ],
                Response::HTTP_NOT_FOUND
            );
        }
    }

    /**
     * @Route("api/v1/posts", name="api_v1_post_create_post", methods={"POST"}, defaults={"_format": "json"})
     *
     * @SWG\Response(
     *     response=201,
     *     description="Create a post for the logged in user.",
     * )
     *
     * @SWG\Parameter(
     *     name="title",
     *     in="body",
     *     type="string",
     *     description="The title",
     *     @SWG\Schema(type="string")
     * )
     *
     * @SWG\Parameter(
     *     name="message",
     *     in="body",
     *     type="string",
     *     description="The message",
     *     @SWG\Schema(type="string")
     * )
     *
     * @SWG\Parameter(
     *     name="attachments",
     *     in="body",
     *     type="array",
     *     description="An array with the attachments. Eg: [{'url' : 'http://url', 'attachmentName': 'picture.jpg'}]",
     *     @SWG\Schema(type="string")
     * )
     *
     * @SWG\Tag(name="Posts")
     * @Security(name="Bearer")
     *
     * @param Request $request
     * @param ApiService $apiService
     *
     * @return JsonResponse
     */
    public function postCreatePost(Request $request, ApiService $apiService): JsonResponse
    {
        $errors = $apiService->createPost($this->getUser(), json_decode($request->getContent(), true));

        if ($errors) {
            return $this->json($errors, Response::HTTP_BAD_REQUEST);
        }

        return $this->json(
            [
                'success' => true,
                'message' => 'The post was successfully created.',
            ],
            Response::HTTP_CREATED
        );
    }
}